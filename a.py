from b import B

class A:
    def __init__(self, a: dict):
        self.a = a["a"]
        self.array_of_b = [B(b) for b in a["array_of_b"]]

    def __str__(self):
        # initialize an empty string 
        str1 = ""  
        
        # traverse in the string   
        for b in self.array_of_b:  
            str1 += b.__str__()

        return self.a + "-> [" + str1 + "]"

from b import B
from a import A


print("Hello World!")
dict_objekti = [
    {
        "a": "a1",
        "array_of_b": [
            {
                "b": "b1"
            },
            {
                "b": "b2"
            },
            {
                "b": "b3"
            }
        ]
    },
    {
        "a": "a2",
        "array_of_b": [
            {
                "b": "1b"
            },
            {
                "b": "2b"
            },
            {
                "b": "3b"
            }
        ]
    }
]


niz_a = [A(a) for a in dict_objekti]

for a in niz_a:
    print(a.__str__())
